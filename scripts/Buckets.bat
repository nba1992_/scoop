echo off
echo # # # # # # # # # # # # #
echo ----- Scoop Buckets -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

echo scoop bucket known
scoop bucket known
echo scoop bucket list
scoop bucket list