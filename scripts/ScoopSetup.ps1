#Note: if you get an error you might need to change the execution policy (i.e. enable Powershell) with
#Set-ExecutionPolicy RemoteSigned -scope CurrentUser

$env:SCOOP='D:\programs\scoop'
[Environment]::SetEnvironmentVariable('SCOOP', $env:SCOOP, 'User')

$env:SCOOP_GLOBAL='D:\programs\scoop\globalApps'
[Environment]::SetEnvironmentVariable('SCOOP_GLOBAL', $env:SCOOP_GLOBAL, 'User')

$env:SCOOP_CACHE='D:\programs\scoop\cache'
[Environment]::SetEnvironmentVariable('SCOOP_CACHE', $env:SCOOP_CACHE, 'User')

Remove-Item $env:SCOOP_CACHE -Recurse
Remove-Item $env:SCOOP_GLOBAL -Recurse
Remove-Item $env:SCOOP -Recurse
New-Item -ItemType "directory" -Path $env:SCOOP
New-Item -ItemType "directory" -Path $env:SCOOP_GLOBAL
New-Item -ItemType "directory" -Path $env:SCOOP_CACHE
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')