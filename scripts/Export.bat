echo off
echo # # # # # # # # # # # # #
echo ----- Scoop Export -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)
set LOG_FILENAME=%~n0.log
echo scoop export > %LOG_FILENAME%
scoop export > %LOG_FILENAME%