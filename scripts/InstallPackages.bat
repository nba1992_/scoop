echo off
echo # # # # # # # # # # # # #
echo ----- Scoop Install Package(s) -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set /p APPS=<apps.txt
echo scoop install %APPS%
scoop install %APPS%

