# Scoop #

This Scoop repository is like DIY to first use this software.

### What is Scoop? ###

* [Scoop](https://scoop.sh/)

### NOTES ###

* Unfortunately, even with non-default folders(D:\\...), you have to execute any of this script with elevated permissions.

### How do I get set up? ###

* Configure variables [SCOOP,SCOOP_GLOBAL,SCOOP_CACHE] at ScoopSetup.ps1 to the desired install dir. 
* Execute ScoopSetup.ps1 in order to install Scoop software.

### How do I get install packages? ###

* Configure apps.txt with the desired software.
* Execute InstallPackages.bat in order to install desired software.

### How do I update packages? ###

* Execute UpdateAllPackages.bat in order to update all scoop packages.

### How do I uninstall packages? ###

* Execute UninstallAllPackages.bat in order to update all scoop packages.
* 
### How do I upgrade Scoop? ###

* Execute ScoopUpgrade.bat.

### Utilities script ###

* Buckets.bat.
* Cache.bat.
* Checkup.bat.
* Cleanup.bat.
* Config.bat.
* Export.bat.
* List.bat.
* Status.bat.

### License ###

FREE!!!

### Review/Changes/Maintenance ###

* Feel free to open PR